require 'bundler/setup'
require 'aws-sdk'

require_relative 'src/podcast_util'
require_relative 'src/model/episode'
require_relative 'config/aws-config'
require_relative 'src/model/aws/dynamo_data_source'
require_relative 'src/model/aws/s3_data_store'
require_relative 'src/ripper/ripper'

Signal.trap 'INT' do
  $ripper.abort
end

Signal.trap 'TERM' do
  $ripper.abort
end

if __FILE__ == $0
  include PodcastApp

  util = PodcastUtil.new
  util.configure_aws

  data_source = DynamoDataSource.new
  data_store = S3DataStore.new data_source
  $ripper = Ripper.new(data_source, data_store)
  $ripper.rip.join
end
