module PodcastApp
  class MostlyWalkingParser
    def parse(video_title, position)
      title = video_title.sub 'Mostly Walking - ', ''
      return title, position + 1
    end
  end
end
