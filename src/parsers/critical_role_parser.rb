module PodcastApp
  class CriticalRoleParser
    def parse(video_title, position)
      title = nil
      track = nil

      # Critical Role was kind enough to be super inconsistent with titling
      if video_title =~ /(?<title>.+)\s+- Critical Role RPG Show\:? Episode (?<number>\d+)/
        title = $~[:title]
        track = $~[:number]
      elsif video_title =~ /(?<title>.+)(, pt\. (?<part>\d+))\s+\|\s+Critical Role RPG Show Episode (?<number>\d+)/
        title = $~[:title]
        track = $~[:number]
        if $~[:part]
          track = (track.to_f + ($~[:part].to_f/10)).to_f
        end
      elsif video_title =~ /(?<title>.+)\s+\|\s+Critical Role RPG( Show)? (LIVE )?Episode (?<number>\d+)(, pt\. (?<part>\d))?/
        title = $~[:title]
        track = $~[:number]
        if $~[:part]
          track = (track.to_f + ($~[:part].to_f/10)).to_f
        end
      end

      if title
        title.strip
      end

      return title, track
    end
  end
end
