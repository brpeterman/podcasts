require 'bundler/setup'
require 'google/api_client'
require 'date'

module PodcastApp
  class YoutubeQuery
    def initialize(api_key)
      @api_key = api_key
      init_api
    end

    def init_api
      @client = Google::APIClient.new(
        :key => @api_key,
        :authorization => nil,
        :application_name => 'PodcastApp',
        :application_version => '1.0.0'
      )
      @youtube = @client.discovered_api('youtube', 'v3')
    end
  end

  def playlist_videos(playlist_id)
    items = []

    next_page = ''
    begin
      response = @client.execute({
        api_method: @youtube.playlist_items.list,
        parameters: {
          playlistId: playlist_id,
          part: 'snippet',
          fields: 'items/snippet/resourceId/videoId,items/snippet/position,nextPageToken',
          pageToken: next_page
        }
      })
      # I know this is not the correct way to deal with this, but Google's
      # documentation is so bad that I can't figure out how to correctly
      # manipulate the result class
      real_result = JSON.parse(response.data.to_json)
      items |= real_result['items']
      next_page = real_result['nextPageToken']
    end until !next_page || next_page == ''

    items.map {|item| {
      id: item['snippet']['resourceId']['videoId'],
      position: item['snippet']['position']
    } }
  end

  def video_info(video_id)
    response = @client.execute({
      api_method: @youtube.videos.list,
      parameters: {
        id: video_id,
        part: 'snippet',
        fields: 'items(snippet(description,publishedAt,title))'
      }
    })
    real_result = JSON.parse(response.data.to_json)

    if (real_result['items'] && real_result['items'].length > 0)
      video  = real_result['items'].first
      return {
        title: video['snippet']['title'],
        description: video['snippet']['description'],
        date: Date.rfc3339(video['snippet']['publishedAt'])
      }
    end
    nil
  end
end
