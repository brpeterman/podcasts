require_relative '../parsers/critical_role_parser'
require_relative '../parsers/mostly_walking_parser'
require_relative 'google/youtube_query'
require_relative '../model/aws/sqs_queue'

module PodcastApp
  class Scanner
    def initialize(data_source, api_key)
      @data_source = data_source
      @api_key = api_key
      @queue = SQSQueue.new

      @episode_factory = PodcastApp::EpisodeFactory.new(data_source, nil)
      setup_parsers
    end

    def setup_parsers
      @parsers = {
        'criticalrole' => CriticalRoleParser.new,
        'mostlywalking' => MostlyWalkingParser.new
      }
    end

    def new_videos(podcast)
      old_episodes = @episode_factory.generate_all(podcast.title)

      videos = @youtube.playlist_videos(podcast.playlist_id)
      videos.reject do |video|
        old_episodes.select {|ep| ep.video_id == video[:id] }.length > 0
      end
    end

    def make_queue_entry(video_id, podcast, title, number, date, description)
      {
        id: video_id,
        podcast: podcast.title,
        title: title,
        number: number,
        date: date,
        description: description
      }.to_json
    end

    def scan(limit=nil)
      @youtube = YoutubeQuery.new @api_key

      podcasts = PodcastFactory.new(@data_source).generate_all
      podcasts.each do |podcast|
        new_videos = new_videos(podcast)
        if limit
          new_videos = new_videos[0..limit-1]
        end
        new_videos.each do |video|
          video_info = @youtube.video_info video[:id]
          title, number = @parsers[podcast.title].parse video_info[:title], video[:position]
          if title && number
            @queue.enqueue make_queue_entry(video[:id], podcast, title, number, video_info[:date].rfc2822, video_info[:description])
          end
        end
      end
    end
  end
end
