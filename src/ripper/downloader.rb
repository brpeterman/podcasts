module PodcastApp
  class Downloader
    BASE_URL = 'https://www.youtube.com/watch'
    CMD = 'youtube-dl -f best --extract-audio --audio-format mp3 --id'

    def initialize(options=nil)

    end

    def download(video_id)
      url = "#{BASE_URL}?v=#{video_id}"
      result = (`#{CMD} #{url}`)
      if (/\[ffmpeg\] Destination: (?<filename>.+?)\n/).match(result)
        filename = $~[:filename]
        size = File.new(filename).size
        return filename, size
      end

      # If we fail to find that line, something went wrong
      return false
    end
  end
end
