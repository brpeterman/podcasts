require_relative 'downloader'
require_relative '../model/aws/sqs_queue'

module PodcastApp
  class Ripper
    def initialize(data_source, data_store)
      @data_source = data_source
      @data_store = data_store
      @downloader = Downloader.new
      @queue = SQSQueue.new

      @rip = true
    end

    def abort
      @rip = false
    end

    def download_video(video_id)
      @downloader.download(video_id)
    end

    def upload_video(video_id, filename, podcast, title, number, date, length, description)
      @data_store.store_episode filename
      episode = Episode.new({
        title: title,
        number: number,
        episode_id: "#{podcast}_#{number}",
        location: filename,
        date: date,
        length: length,
        video_id: video_id,
        description: description
      })
      @data_source.add_episode podcast, episode
    end

    def cleanup_video(filename)
      File.unlink(filename)
    end

    def rip(limit=nil)
      Thread.new do
        begin
          message, receipt_handle = @queue.next_message

          if message
            filename, length = download_video message['id']
            if filename
              upload_video(
                message['id'],
                filename,
                message['podcast'],
                message['title'],
                message['number'],
                Date.rfc3339(message['date']),
                length,
                message['description']
              )
              cleanup_video filename
              @queue.delete_message receipt_handle
            end
          end
        end while @rip
      end
    end
  end
end
