module PodcastApp
  class Podcast
    # [title] is a URL-friendly identifier
    # [full_title] is the actual title of the podcast
    attr_reader :title,
                :full_title,
                :author,
                :url,
                :description,
                :language,
                :itunes_metadata,
                :episodes,
                :playlist_id,
                :icon

    def initialize(attributes)
      attributes.each do |key, value|
        self.instance_variable_set "@#{key}", value
      end
    end

    def to_rss(episodes, token=nil)
      rss = RSS::Rss.new('2.0')

      query_string = ""
      if token
        query_string = "?token=#{token}"
      end

      author = @author
      title = @full_title
      homepage = @url

      channel = RSS::Rss::Channel.new

      channel.title = title
      channel.lastBuildDate = Date.today.rfc2822
      channel.description = @description if @description
      channel.link = homepage
      channel.language = 'en-us'

      if @icon
        channel.image = RSS::Rss::Channel::Image.new
        channel.image.url = @icon
        channel.image.title = title
        channel.image.link = homepage
      end

      if @itunes_metadata
        channel.itunes_author = @itunes_metadata['author']
        channel.itunes_owner = RSS::ITunesChannelModel::ITunesOwner.new
        if @itunes_metadata['owner']
          owner_name = @itunes_metadata['owner']['name']
          owner_email = @itunes_metadata['owner']['email']
          channel.itunes_owner.itunes_name = owner_name if owner_name
          channel.itunes_owner.itunes_email = owner_email if owner_email
        end

        if @itunes_metadata['category']
          category = RSS::ITunesChannelModel::ITunesCategory.new(@itunes_metadata['category'])
          channel.itunes_categories << category
        end

        channel.itunes_summary = @description if @description

        channel.itunes_image = RSS::ITunesChannelModel::ITunesImage.new(@icon) if @icon
        channel.itunes_explicit = @itunes_metadata['explicit']
      end

      episodes.each do |episode|
        item = RSS::Rss::Channel::Item.new

        link = episode.guid + query_string

        item.title = "#{episode.number} - #{episode.title}"
        item.description = episode.description
        item.link = link
        item.guid = RSS::Rss::Channel::Item::Guid.new
        item.guid.content = episode.guid
        item.guid.isPermaLink = false
        item.pubDate = episode.date.rfc2822
        item.enclosure = RSS::Rss::Channel::Item::Enclosure.new(link, episode.length, 'audio/mpeg')

        channel.items << item
      end

      rss.channel = channel

      rss
    end

    def to_s
      "<PodcastApp::Podcast>[title:#{@title}][full_title:#{@full_title}]"
    end
  end
end
