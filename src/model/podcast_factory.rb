require_relative 'podcast'

module PodcastApp
  class PodcastFactory
    def initialize(data_source)
      @data_source = data_source
    end

    def generate(title)
      attributes = @data_source.podcast title
      Podcast.new attributes
    end

    def generate_all()
      attributes = @data_source.all_podcasts
      attributes.map do |attr|
        Podcast.new attr
      end
    end
  end
end
