module PodcastApp
  class S3DataStore
    def initialize(data_source)
      @s3 = Aws::S3::Resource.new({ region: PodcastApp::Config::Aws::S3::Region })
      @data_source = data_source
    end

    def episode(location)
      bucket_name = PodcastApp::Config::Aws::S3::Bucket
      bucket = @s3.bucket bucket_name
      file = bucket.object location
      file.presigned_url :get, {
        expires_in: 604800
      }
    end

    def store_episode(filename)
      bucket_name = PodcastApp::Config::Aws::S3::Bucket
      name = File.basename filename
      s3_obj = @s3.bucket(bucket_name).object(name)
      s3_obj.upload_file(filename, {
        acl: 'private'
      })
    end
  end
end
