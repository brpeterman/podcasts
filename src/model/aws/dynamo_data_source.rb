module PodcastApp
  class DynamoDataSource
    def initialize()
      @db = Aws::DynamoDB::Resource.new({ region: PodcastApp::Config::Aws::DynamoDB::Region })
    end

    def podcast(title)
      $stderr.puts "Looking up a podcast with title [#{title}]"
      table = @db.table 'podcasts'
      query = table.get_item({
        :key => {'title' => title}
      })

      {
        title: title,
        full_title: query.item['full_title'],
        author: query.item['author'],
        description: query.item['description'],
        language: query.item['language'],
        url: query.item['url'],
        location: query.item['location'],
        itunes_metadata: query.item['itunes_metadata'],
        playlist_id: query.item['playlist_id'],
        icon: query.item['icon']
      }
    end

    def all_podcasts()
      table = @db.table 'podcasts'
      query = table.scan

      query.items.map do |item|
        {
          title: item['title'],
          full_title: item['full_title'],
          author: item['author'],
          description: item['description'],
          language: item['language'],
          url: item['url'],
          location: item['location'],
          itunes_metadata: item['itunes_metadata'],
          playlist_id: item['playlist_id'],
          icon: item['icon']
        }
      end
    end

    def episode(title, episode_number)
      episode_id = "#{title}_#{episode_number}"

      table = @db.table 'episodes'
      query = table.get_item({
        key: {'episode_id' => episode_id}
      })

      {
        title: query.item['title'],
        number: episode_number,
        location: query.item['location'],
        length: query.item['length'],
        date: Date.rfc2822(query.item['date']),
        video_id: query.item['video_id'],
        description: query.item['description'],
        podcast: query.item['podcast']
      }
    end

    # Fetch all episodes in a single podcast.
    # Episodes will not necessarily be sorted.
    def all_episodes(title)
      table = @db.table 'episodes'
      query = table.scan({
        expression_attribute_values: {
          ':title' => title
        },
        filter_expression: 'podcast = :title'
      })

      # The format that DynamoDB returns and the format I want to pass back
      # are identical, but I want to be explicit about it in case of
      # future changes
      query.items.map do |item|
        {
          title: item['title'],
          number: item['number'],
          location: item['location'],
          length: item['length'],
          date: Date.rfc2822(item['date']),
          video_id: item['video_id'],
          description: item['description'],
          podcast: item['podcast']
        }
      end
    end

    def add_episode(podcast_title, episode)
      table = @db.table 'episodes'
      table.put_item({
        item: {
          'episode_id' => "#{podcast_title}_#{episode.number}",
          'title' => episode.title,
          'number' => episode.number,
          'location' => episode.location,
          'length' => episode.length,
          'date' => episode.date.rfc2822,
          'video_id' => episode.video_id,
          'podcast' => podcast_title,
          'description' => episode.description
        }
      })
    end
  end
end
