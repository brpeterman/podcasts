module PodcastApp
  class SQSQueue
    def initialize
      @sqs = Aws::SQS::Client.new(
        region: PodcastApp::Config::Aws::SQS::Region
      )
    end

    def enqueue entry
      @sqs.send_message({
        queue_url: PodcastApp::Config::Aws::SQS::Uri,
        message_body: entry,
        message_group_id: 'episodes'
      })
    end

    def next_message
      result = @sqs.receive_message({
        queue_url: PodcastApp::Config::Aws::SQS::Uri,
        max_number_of_messages: 1,
        wait_time_seconds: 10
      })
      message = result.messages.first
      if message
        return JSON.parse(message.body), message.receipt_handle
      end
    end

    def delete_message(receipt_handle)
      @sqs.delete_message({
        queue_url: PodcastApp::Config::Aws::SQS::Uri,
        receipt_handle: receipt_handle
      })
    end
  end
end
