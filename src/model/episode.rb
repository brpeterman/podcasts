module PodcastApp
  class Episode

    attr_reader :title,
                :episode_id,
                :location,
                :length,
                :date,
                :video_id,
                :description,
                :podcast

    def initialize(attributes, data_store=nil)
      @data_store = data_store

      attributes.each do |key, value|
        self.instance_variable_set "@#{key}", value
      end
    end

    def resource
      @data_store.episode @location
    end

    def number
      '%g' % ('%.1f' % @number)
    end

    def guid
      "http://podcasts.bpeterman.com/#{@podcast}/episode/#{number}"
    end
  end
end
