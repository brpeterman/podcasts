module PodcastApp
  class EpisodeFactory
    def initialize(data_source, data_store)
      @data_source = data_source
      @data_store = data_store
    end

    def generate(title, episode_number)
      attributes = @data_source.episode(title, episode_number)
      Episode.new attributes, @data_store
    end

    def generate_all(title)
      episode_attrs = @data_source.all_episodes(title)
      episode_attrs.map do |attr|
        Episode.new attr, @data_store
      end
    end
  end
end
