module PodcastApp
  class PodcastUtil
    attr_accessor :data_source

    def initialize

    end

    def configure_aws
      if defined? PodcastApp::Config::Aws::AccessID
        Aws.config.update({
                            region: 'us-east-1',
                            credentials: Aws::Credentials.new(PodcastApp::Config::Aws::AccessID, PodcastApp::Config::Aws::AccessKey)
                          })
      end
    end

    def is_podcast(title)
      @data_source.podcast(title) != nil
    end

    def has_episode(title, episode_number)
      !!@data_source.episode(title, episode_number) != nil
    end

    def authenticated? token
      if Config::Tokens.values.include? token
        true
      end
    end
  end
end
