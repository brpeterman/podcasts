require 'json'
require 'aws-sdk'

require_relative 'lib/model/aws/dynamo_data_source'
require_relative 'lib/model/aws/s3_data_store'
require_relative 'lib/model/podcast_factory'
require_relative 'lib/model/episode_factory'
require_relative 'lib/model/podcast'
require_relative 'lib/model/episode'
require_relative 'lib/podcast_util'
require_relative 'config/aws-config.rb'
require_relative 'config/app-config.rb'

include PodcastApp

event_json = ARGV[0]

if !event_json
  $stdout.puts 'No arguments'
  exit 1
end

event = JSON.parse event_json
title = event['podcastId']
episodeNum = event['episodeNum']
token = event['token']

util = PodcastUtil.new
util.configure_aws

data_source = DynamoDataSource.new
data_store = S3DataStore.new data_source
episode_factory = EpisodeFactory.new data_source, data_store

util.data_source = data_source

if !util.authenticated? token
  $stdout.puts 'Not authorized'
  exit 2
end

if util.is_podcast(title) && util.has_episode(title, episodeNum)
  episode = episode_factory.generate(title, episodeNum)
  $stdout.print episode.resource
  exit 0
end

$stdout.puts 'Invalid arguments'
exit 3
