#!/bin/bash

set -e

# Figure out where this script is located.
SELFDIR="`dirname \"$0\"`"
SELFDIR="`cd \"$SELFDIR\" && pwd`"

# Tell Bundler where the Gemfile and gems are.
cp -r "$SELFDIR/lib/vendor" /tmp
export BUNDLE_GEMFILE="/tmp/vendor/Gemfile"
export BUNDLE_APP_CONFIG="/tmp/vendor/.bundle"
unset BUNDLE_IGNORE_CONFIG

# Run the actual app using the bundled Ruby interpreter, with Bundler activated.
exec "$SELFDIR/lib/ruby/bin/ruby" -rbundler/setup "$@"
