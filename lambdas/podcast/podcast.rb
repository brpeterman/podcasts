require 'json'
require 'rss'
require 'aws-sdk'

require_relative 'lib/model/aws/dynamo_data_source'
require_relative 'lib/model/aws/s3_data_store'
require_relative 'lib/model/podcast_factory'
require_relative 'lib/model/episode_factory'
require_relative 'lib/model/podcast'
require_relative 'lib/model/episode'
require_relative 'lib/podcast_util'
require_relative 'config/aws-config.rb'
require_relative 'config/app-config.rb'

include PodcastApp

event_json = ARGV[0]

if !event_json
  exit 1
end

event = JSON.parse event_json
title = event['podcastId']
token = event['token']

util = PodcastUtil.new
util.configure_aws

if !util.authenticated? token
  exit 2
end

data_source = DynamoDataSource.new
data_store = S3DataStore.new data_source
podcast_factory = PodcastFactory.new data_source
episode_factory = EpisodeFactory.new data_source, data_store

util.data_source = data_source

if util.is_podcast(title)
  podcast = podcast_factory.generate(title)
  episodes = episode_factory.generate_all(title).sort {|a, b| b.number.to_f <=> a.number.to_f}

  rss = podcast.to_rss(episodes, token)
  $stdout.puts rss.to_s
  exit 0
else
  exit 1
end
