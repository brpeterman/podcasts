const exec = require('child_process').exec;

exports.handler = function(event, context, callback) {
  console.log("Executing: ./wrapper.sh scanner.rb");
  const child = exec('./wrapper.sh scanner.rb');

  var output = '';
  child.stderr.on('data', (data) => {
    console.log("STDERR: " + data);
  });
  child.stdout.on('data', (data) => {
    console.log("STDOUT: " + data);
    output += data.toString();
  });
  child.on('exit', (result) => {
    if (result !== 0) {
      console.log("Fail (" + result + "): " + output);
      callback(new Error(output));
    }
    else {
      console.log("Success: " + output);
      callback(null, output);
    }
  });
};
