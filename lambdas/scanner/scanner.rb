require 'aws-sdk'

require_relative 'lib/podcast_util'
require_relative 'config/aws-config'
require_relative 'config/google-config'
require_relative 'lib/model/podcast_factory'
require_relative 'lib/model/episode_factory'
require_relative 'lib/model/podcast'
require_relative 'lib/model/episode'
require_relative 'lib/model/aws/dynamo_data_source'
require_relative 'lib/scanner/scanner'

include PodcastApp

util = PodcastUtil.new
util.configure_aws

data_source = DynamoDataSource.new
Scanner.new(data_source, PodcastApp::Config::Google::ApiKey).scan

exit 0
