require 'bundler/setup'

VERSION = '1.0.0'
TRAVELING_RUBY_VERSION = '20150715-2.2.2'
TARGET = 'linux-x86_64'
TRAVELING_RUBY_PATH = "packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-#{TARGET}.tar.gz"
LAMBDAS_DIR = 'lambdas'
PODCAST_LIB_DIR = 'src'
PACKAGE_DIR = 'tmp'
BUILD_DIR = 'build'
LAMBDAS = [:scanner, :episode, :podcast]
DEFAULT_ENV = 'development'

task :default => [:clean, :env_setup, :bundle_install, :compile_lambdas]

task :ripper do
  puts "Packaging Ripper..."
end

task :scanner => [:env_setup, :bundle_install] do
  puts "Packaging Scanner Lambda..."
  package 'scanner'
end

task :episode => [:env_setup, :bundle_install] do
  puts "Packaging Episode Lambda..."
  package 'episode'
end

task :podcast => [:env_setup, :bundle_install] do
  puts "Packaging Podcast Lambda..."
  package 'podcast'
end

task :lambdas => [:clean, :env_setup, :bundle_install, :scanner, :episode, :podcast]

task :clean do
  clean
end

task :compile_lambdas do
  puts "Compiling all Lambdas..."
  LAMBDAS.each do |lambda|
    build lambda.to_s, true
  end
end

task :env_setup do
  if !ENV['RAKE_ENV'] || ENV['RAKE_ENV'] == ''
    ENV['RAKE_ENV'] = DEFAULT_ENV
  end
end

task :bundle_install do
  if RUBY_VERSION !~ /^2\.2\./
    abort "You can only 'bundle install' using Ruby 2.2, because that's what Traveling Ruby uses."
  end
  sh "rm -rf packaging/tmp"
  sh "mkdir packaging/tmp"
  sh "cp Gemfile Gemfile.lock packaging/tmp"
  Bundler.with_clean_env do
    sh "cd packaging/tmp && env BUNDLE_IGNORE_CONFIG=1 bundle install --path ../vendor --without development"
  end
  sh "rm -rf packaging/tmp"
  sh "rm -f packaging/vendor/*/*/cache/*"
end

def build(lambda_name, include_aws=false)
  puts "Building #{lambda_name}..."
  package_ruby
  package_lambda(lambda_name, include_aws)
end

def package(lambda_name)
  puts "Packaging #{lambda_name}"
  package_ruby
  package_lambda(lambda_name)
  sh "mkdir -p #{BUILD_DIR}"
  sh "cd #{PACKAGE_DIR}; zip -r ../#{BUILD_DIR}/#{lambda_name}.zip *; cd .."
  clean
end

def package_ruby
  sh "mkdir -p #{PACKAGE_DIR}"
  sh "mkdir -p #{PACKAGE_DIR}/lib/ruby"
  sh "tar -xzf packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-#{TARGET}.tar.gz -C #{PACKAGE_DIR}/lib/ruby"
  sh "mkdir -p packaging/vendor"
  sh "cp -pR packaging/vendor #{PACKAGE_DIR}/lib/"
  sh "cp Gemfile Gemfile.lock #{PACKAGE_DIR}/lib/vendor/"
  sh "mkdir -p #{PACKAGE_DIR}/lib/vendor/.bundle"
  sh "cp packaging/bundler-config #{PACKAGE_DIR}/lib/vendor/.bundle/config"
end

def package_lambda(lambda_name, include_aws=false)
  sh "mkdir -p #{PACKAGE_DIR}"
  sh "cp -r #{LAMBDAS_DIR}/#{lambda_name}/* #{PACKAGE_DIR}"
  sh "mkdir -p #{PACKAGE_DIR}/config"
  if ENV['RAKE_ENV'] == 'development'
    sh "cp -r config/development/* #{PACKAGE_DIR}/config"
  elsif ENV['RAKE_ENV'] == 'production'
    sh "cp -r config/production/* #{PACKAGE_DIR}/config"
  end
  sh "mkdir -p #{PACKAGE_DIR}/lib"
  sh "cp -r #{PODCAST_LIB_DIR}/* #{PACKAGE_DIR}/lib"
  sh "cp lambdas/wrapper.sh #{PACKAGE_DIR}"
end

def clean
  sh "rm -rf #{PACKAGE_DIR}"
end
